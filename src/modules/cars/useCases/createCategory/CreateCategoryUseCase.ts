import { ICategoriesRepository } from '../../repositories/ICategoriesRepository'

interface IRerquest {
    name: string
    description: string
}

class CreateCategoryUseCase{

	constructor(private categoriesRepository: ICategoriesRepository) {}

	execute({ name, description }: IRerquest): void{

		const categoryAllReadyExists = this.categoriesRepository.findByName(name)

		if(categoryAllReadyExists){
			throw new Error('Category Already exists')
		}

		this.categoriesRepository.create({name, description})
	}
}

export { CreateCategoryUseCase }